export const state = () => ({
  products: [],
  product: null,
  cart: [],
  productsCount: 0,
  minPrice: 0,
  maxPrice: 100000,
})

export const mutations = {
  ADD_TO_CART(state, {product, quantity}) {
    let productItemInCart = state.cart.find((item) => {
      return item.product.id === product.id
    })

    if(productItemInCart) {
      productItemInCart.quantity += quantity
      return
    }
    this.state.cart.push({
      product,
      quantity
    })
  },
}

export const getters = {

}

export const actions = {
  addProductToCart({commit}, {product, quantity}) {
    commit('ADD_TO_CART', {product, quantity})
  },
}
