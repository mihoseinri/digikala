export default function ({ $axios }, inject) {
  // Create a custom axios instance
  const api = $axios;
  api.setBaseURL('https://www.digikala.com/front-end/')
  api.setHeader('token', 'mpfKW9ghVTCSuBZ7qTkSmEyvL38ShZxv')

  // Inject to context as $api
  inject('api', api)
}
